package com.pmdTeam2.repository;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.pmdTeam2.entity.XmlFile;
import com.pmdTeam2.entity.report;

@Repository
public interface reportRepository extends PagingAndSortingRepository<report, Long>, JpaRepository<report, Long> {
	@Query(value = "SELECT r.* FROM report r JOIN xmlfile x on r.xml_file_id = x.id where x.id=?1", nativeQuery = true)
	public List<report> findByIdxmlfile(long id);

	@Query(value = "SELECT x.* FROM xmlfile x JOIN report r on r.xml_File_id = x.id JOIN logupload l on l.id = x.log_upload_id  where l.id=?1", nativeQuery = true)
	XmlFile findById(long id);

	@Query(value = "SELECT r.* FROM xmlfile x JOIN report r on r.xml_File_id = x.id JOIN logupload l on l.id = x.log_upload_id  where l.id=?1", nativeQuery = true)
	List<report> findAllreportbyid(long id);

	@Query(value = "SELECT r.* FROM xmlfile x JOIN report r on r.xml_File_id = x.id  where x.id=?1", nativeQuery = true)
	Page<List<report>> findreport(long id, Pageable pageable);

}

