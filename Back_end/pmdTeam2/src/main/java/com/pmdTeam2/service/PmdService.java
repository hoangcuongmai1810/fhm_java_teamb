package com.pmdTeam2.service;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.pmdTeam2.entity.LogUpload;
import com.pmdTeam2.entity.User;
import com.pmdTeam2.entity.XmlFile;
import com.pmdTeam2.entity.report;
import com.pmdTeam2.repository.LogUploadRepository;
import com.pmdTeam2.repository.UserRepository;
import com.pmdTeam2.repository.XmlFileRepository;
import com.pmdTeam2.repository.reportRepository;

import net.sourceforge.pmd.PMD;

@Service
public class PmdService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private reportRepository reportRepo;

	@Autowired
	private LogUploadRepository logUploadRepository;

	@Autowired
	private XmlFileRepository xmlFileRepository;

	public static XmlFile filexml;
	public static long idc;
	
	public void exportXML(String fileName) {

		String javaFile = fileName;
		String xmlFile = javaFile.substring(0, javaFile.lastIndexOf('.'));

		String[] arguments = { "-d", "D:\\updown\\" + javaFile, "-f", "xml", "-R", "D:\\javarule.xml", "-r",
				"D:\\updown\\" + xmlFile + ".xml" };
		PMD.run(arguments);
		LogUpload logUpload = logUploadRepository.findLogUploadByFileName("D:\\updown\\" + javaFile);
		System.out.println(logUpload.getId());
		XmlFile xmlFile2 = new XmlFile("D:\\updown\\" + xmlFile + ".xml", logUpload);
		XmlFile xmlf;
		xmlf = xmlFileRepository.save(xmlFile2);
		idc = xmlf.getId();
		
		try {
			filexml = xmlf;
			
			//filename(xmlf);
			report(xmlf);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public ResponseEntity<Object> uploadFile(MultipartFile file) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = authentication.getName();
		User user = userRepository.findByUsername(username).get();
		String pattern = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

		String date = simpleDateFormat.format(new Date());
		String getFileName = date.toString().replaceAll("[^a-zA-Z0-9]", "_") + "_" + file.getOriginalFilename();
		// System.out.println(date.toString().replaceAll("[^a-zA-Z0-9]", ""));
		File uploadedFile = new File("D:\\updown", getFileName);

		String fileName = "D:\\updown\\" + getFileName;
		try {
			uploadedFile.createNewFile();
			FileOutputStream fileOutputStream = new FileOutputStream(uploadedFile);
			fileOutputStream.write(file.getBytes());
			fileOutputStream.close();
			LogUpload logUpload = new LogUpload(fileName, user);
			logUploadRepository.save(logUpload);
			exportXML(getFileName);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new ResponseEntity<Object>("file Uplaoded succesfully", HttpStatus.OK);
	}
	
	

	// read xml file to DB
	
	public static List<String> listpath;
//	public void filename(XmlFile idXml) throws ParserConfigurationException, SAXException, IOException {
//		try {
//			
//			File f = new File(idXml.getFilename());
//			System.out.println(f);
//			if(f.exists()) {
//				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
//				DocumentBuilder buider = factory.newDocumentBuilder();
//				Document doc = buider.parse(f);
//
//				Element files = doc.getDocumentElement();
//				
//				NodeList fileList = files.getElementsByTagName("file");
//
//				for(int i = 0; i < fileList.getLength(); i++) {
//					Node node1= fileList.item(i);
//					
//					if(node1.getNodeType() == Node.ELEMENT_NODE) {
//						
//						Element filcontent = (Element) node1;
//						String path =filcontent.getAttribute("name");
//						listpath.add(path);
//						System.out.println(path);
//						System.out.println("read path success");
//						
//					}
//				}
//			}
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//			System.out.println("No files");
//		}
//		
//		
//		
//		
//	}
	
	
	public void report(XmlFile idXml) throws ParserConfigurationException, SAXException, IOException {
		File f = new File(idXml.getFilename());
		
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder buider = factory.newDocumentBuilder();
		Document doc = buider.parse(f);

		Element file = doc.getDocumentElement();
		NodeList violationList = file.getElementsByTagName("violation");

		report b = new report();
		
		NodeList fileList = file.getElementsByTagName("file");

		for(int j = 0; j < fileList.getLength(); j++) {
			Node node1= fileList.item(j);
			
			if(node1.getNodeType() == Node.ELEMENT_NODE) {
				
				Element filcontent = (Element) node1;
				String path =filcontent.getAttribute("name");

				for (int i = 0; i < violationList.getLength(); i++) {
					Node node = violationList.item(i);
				
					if (node.getNodeType() == Node.ELEMENT_NODE) {
				
						

						Element violation = (Element) node;
				
						String beginline = violation.getAttribute("beginline");
						String endline = violation.getAttribute("endline");
						String begincolumn = violation.getAttribute("begincolumn");
						String endcolumn = violation.getAttribute("endcolumn");
						String rule = violation.getAttribute("rule");
						String ruleset = violation.getAttribute("ruleset");
						String packages = violation.getAttribute("package");
						String class_z = violation.getAttribute("class");
						String temp= violation.getTextContent();
						String externalInfoUrl = violation.getAttribute("externalInfoUrl");
						String priority = violation.getAttribute("priority");
						b = new report(beginline, endline, begincolumn, endcolumn, rule,path,  ruleset, packages, class_z,
								externalInfoUrl,temp, idXml, priority);
					}

					reportRepo.save(b);
				}
				
			}
		}
		
	
		
		
	}
	
	

}
