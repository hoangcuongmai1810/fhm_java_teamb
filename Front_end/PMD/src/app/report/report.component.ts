import { PageService } from './../service/page/page.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {
  btnfilter:string = '0';
  selectedDevice;
  sorttype:string = 'NO';
  isSort:boolean = false;
  reports: any = [];
  rps: any = [];
  id: any;
  fileName: string;
  fileSource:string;
  page:number = 1;
  totalPage:any;
  selected:number;

  httpOptions = {
    headers: new HttpHeaders().set('Authorization', 'Bearer ' + sessionStorage.getItem('accessToken'))
  };

  constructor(private pageSV: PageService, private HttpClient: HttpClient, private route: ActivatedRoute, private router: Router) {
    const id = this.route.snapshot.params['id'];
    this.id = id;
  }

   ngOnInit() {
    this.run(this.id);
    this.pagination(this.id, this.page, this.selectedDevice);
  }

  public run(id) {
    this.getFileSource(id);
    this.getFileName(id);
  }
  
  //  filter
  getType($event){
  this.btnfilter = $event;
  this.selectedDevice = $event;
  this.pagination(this.id, this.page, this.selectedDevice);
  }

  pagination(id, page, selectedDevice) {
    this.HttpClient.get('http://localhost:4300/api/file/paging/'+id + '/' +page + '/' + selectedDevice, this.httpOptions).subscribe(rp =>{
      this.reports = rp[1];
      this.totalPage = rp[0];
      console.log(rp);
      this.selected = page;
      this.page = page;
       
    })
  }

  // get filename
  getFileName(id) {
    this.pageSV.getName(id).subscribe(name => {
      let str = name['filename'].substr(11);
      let str1 = str.substr(str.lastIndexOf("."));
      str = str.replace(str1, "");
      this.fileName = str;
    })
  }
  getFileSource(id) {
    this.pageSV.getName(id).subscribe(name => {
      this.fileSource = name['filename'].substr(11);
    })
  }

  // download file
  downloadFile(fileName): void {
    this.pageSV.download(this.id).subscribe(
      (response: any) => {
        let dataType = response.type;
        let binaryData = [];
        binaryData.push(response);
        let downloadLink = document.createElement('a');
        downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, { type: dataType }));
        if (fileName)
          this.fileName = fileName;
        downloadLink.setAttribute('download', this.fileName);
        document.body.appendChild(downloadLink);
        downloadLink.click();
      }
    )
  }

}
