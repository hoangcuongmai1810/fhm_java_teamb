import { AuthService } from './service/auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'PMD Tool';

  constructor(private auth:AuthService){}

  ngOnInit(){
  }
  
  logout(){
    this.auth.logoutSV();
  }
}
