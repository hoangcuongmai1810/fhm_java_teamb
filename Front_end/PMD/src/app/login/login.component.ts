import { AuthService } from './../service/auth/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username = '';
  password = '';
  returnUrl: string;

  constructor(private router: Router, private httpClient: HttpClient, private auth: AuthService) {
      
   }

  ngOnInit() {
  }

    login() {
        this.auth.postLogin(this.username, this.password).subscribe(data =>{
          sessionStorage.setItem('accessToken', data['accessToken']);
          this.router.navigate(['/upload']);
        });
    }

}
