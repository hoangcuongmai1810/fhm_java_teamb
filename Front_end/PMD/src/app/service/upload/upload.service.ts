import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  constructor(private http: HttpClient) { }

  apiUpload: string = 'http://localhost:4300/api/file/logs';
  apiUploadFile: string = 'http://localhost:4300/api/file/uploadFile';

  httpOptions = {
    headers: new HttpHeaders().set('Authorization', 'Bearer ' + sessionStorage.getItem('accessToken'))
  };


  // get list
  public getListLog() {
    return this.http.get(this.apiUpload, this.httpOptions);
  }

  //upload
  postFile(fileToUpload: File) {
    const endpoint = this.apiUploadFile;
    const formData: FormData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    return this.http.post(endpoint, formData, this.httpOptions)
  }
}
