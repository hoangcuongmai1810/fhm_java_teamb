import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private myRoute: Router, private http:HttpClient) { }
  
  apiLogin:string = 'http://localhost:4300/api/auth/signin';
  headers = new HttpHeaders({ 'Content-Type':  'application/json' });

  postLogin(username, password){
    return this.http.post(this.apiLogin, {'username': username, 'password':password} ,{headers: this.headers})
  }

  getToken() {
    return sessionStorage.getItem("accessToken");
  }

  isLoggedIn() {
    return this.getToken() !== null;
  }

  logoutSV(){
    sessionStorage.removeItem('accessToken');
  }
}
